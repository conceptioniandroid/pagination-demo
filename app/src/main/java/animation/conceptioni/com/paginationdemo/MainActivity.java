package animation.conceptioni.com.paginationdemo;

import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import animation.conceptioni.com.paginationdemo.database.DBOpenHelper;

public class MainActivity extends AppCompatActivity{
    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    RecyclerviewAdapter recyclerviewAdapter;
    ArrayList<Model> modelsArray = new ArrayList<>();
    int intPage=1;
    protected Handler handler;
    Paginate paginate;
    ProgressDialog progressDialog;
    private boolean isloading = false;
    ProgressBar progressBar;
    TextView tvNoData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();
        init();
    }

    private void init() {
        tvNoData =  findViewById(R.id.tvNoData);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        recyclerview = findViewById(R.id.recyclerview);
        linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerview.setLayoutManager(linearLayoutManager);
        CallProductList();

    }
    public static class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.progress_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        }
    }


    static class VH extends RecyclerView.ViewHolder {

        public VH(View itemView) {
            super(itemView);
        }
    }

    public void CallProductList() {
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("API","get_product_list");
        hashMap.put("category_id","1");
        hashMap.put("page",""+intPage);
        Log.d("++jsonplist","++ "+hashMap.toString());
        try {
            progressDialog=new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            new CallWebService(Constants.method.POST,Constants.productListUrl,hashMap) {
                @Override
                public void OnStartingService() {
                    super.OnStartingService();
                }
                @Override
                public void OnGettingResult(JSONObject jsonObject) throws JSONException, MethodNotDefinedException {
                    super.OnGettingResult(jsonObject);
                    Log.d("++jsonplist","+++"+jsonObject);
                    if (jsonObject!=null) {
                        try {
                            if (intPage == 1) {
                                modelsArray.clear();
                            }
                            if (jsonObject.getString(Constants.webServiceParam.success).equalsIgnoreCase("1")){
                                tvNoData.setVisibility(View.GONE);
                                recyclerview.setVisibility(View.VISIBLE);
                                JSONArray jsonArray=jsonObject.getJSONArray("productlist");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object=jsonArray.getJSONObject(i);
                                    Model model =new Model();
                                    model.setTitle(object.getString("title"));
                                    modelsArray.add(model);
                                }
                                if (intPage == 1) {
                                    recyclerviewAdapter = new RecyclerviewAdapter(modelsArray);
                                    recyclerview.setAdapter(recyclerviewAdapter);

                                    paginate = Paginate.with(recyclerview, callbacks)
                                            .setLoadingTriggerThreshold(2)
                                            .addLoadingListItem(true)
                                            .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                                            .build();
                                } else {
                                    recyclerviewAdapter.add(modelsArray);
                                    isloading = false;
                                }
                            }else {
                                tvNoData.setVisibility(View.VISIBLE);
                                recyclerview.setVisibility(View.GONE);
                                if (paginate != null) {
                                    paginate.setHasMoreDataToLoad(false);
                                    paginate.unbind();

                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else{
                        tvNoData.setVisibility(View.VISIBLE);
                        recyclerview.setVisibility(View.GONE);
                        if (paginate != null) {
                            paginate.setHasMoreDataToLoad(false);
                            paginate.unbind();
                        }
                    }
                }

                @Override
                public void InternetConnectionNotAvailable() {
                    super.InternetConnectionNotAvailable();
                    Toast.makeText(MainActivity.this,"Please check your internet conncetion",Toast.LENGTH_SHORT);
                }

                @Override
                public void InternetConnectionAvailable() {
                    super.InternetConnectionAvailable();
                }
            };
        } catch (MethodNotDefinedException e) {
            e.printStackTrace();
        }
    }

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            intPage++;
            isloading = true;
            // Fake asynchronous loading that will generate page of random data after some delay
            handler.post(fakeCallback);
        }

        @Override
        public boolean isLoading() {
            return isloading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return intPage == 10000;
        }
    };
    private Runnable fakeCallback = new Runnable() {
        @Override
        public void run() {
            CallProductList();
        }
    };
}
