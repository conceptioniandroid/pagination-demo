package animation.conceptioni.com.paginationdemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;

import animation.conceptioni.com.paginationdemo.Model;

public class DBOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Pagination.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_ADD_DATA = "ADD_DATA";
    public static final String int_glcode = "int_glcode";
    public static final String Title = "title";

    Context context;

    private static final String CREATE_ADD_DATA_TABLE = " CREATE TABLE " + TABLE_ADD_DATA
            + "(" + int_glcode + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Title + " TEXT "
            + ") ";

    private ArrayList<Model> modelsArray = new ArrayList<>();

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ADD_DATA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADD_DATA);
        onCreate(db);
    }

    public void addData(Integer title
    ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Title, title);
        db.insert(TABLE_ADD_DATA, null, contentValues);
        Log.d("+++success", "++++" + contentValues.toString());
    }

    public ArrayList getData() {
        modelsArray.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_ADD_DATA ,null);
        Log.d("+++++++++++count", "++++++" + res.getCount());
        try {
            if (res.getCount() > 0) {
                res.moveToFirst();
                for (int i = 0; i < res.getCount(); i++) {
                    Model model = new Model();
                    model.setId(res.getInt(0));
                   // model.setTitle(res.getInt(1));
                    modelsArray.add(model);
                    res.moveToNext();
                }
            }
        }finally {
            if (res != null && !res.isClosed())
                res.close();
        }
        Log.d("+++report", String.valueOf(modelsArray.size()));
        return modelsArray;
    }

}
