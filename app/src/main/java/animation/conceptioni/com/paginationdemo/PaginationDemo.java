package animation.conceptioni.com.paginationdemo;

import android.app.Application;
import android.content.Context;

/**
 * Created by 123 on 21-02-2017.
 */

public class PaginationDemo extends Application {

    private static PaginationDemo mInstance;

    public PaginationDemo(){
        mInstance=this;
    }


    public static final String TAG = PaginationDemo.class.getSimpleName();

    public static Context getContext(){return mInstance;}

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
