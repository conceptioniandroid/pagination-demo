package animation.conceptioni.com.paginationdemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerviewAdapter extends RecyclerView.Adapter<RecyclerviewAdapter.Holder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Model> modelsArray = new ArrayList<>();

    public RecyclerviewAdapter(ArrayList<Model> modelsArray){
        this.modelsArray = modelsArray;
    }
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_item,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.tvTitle.setText(modelsArray.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return modelsArray.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        public Holder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
    public void add(ArrayList<Model> items) {
        notifyItemInserted(items.size());
    }
}
