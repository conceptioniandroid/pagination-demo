package animation.conceptioni.com.paginationdemo;

public class MethodNotDefinedException extends Exception {

    public MethodNotDefinedException(){

    }

    public MethodNotDefinedException(String Message){
        super(Message);
    }

}
