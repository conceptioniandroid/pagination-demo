package animation.conceptioni.com.paginationdemo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


public class CallWebService {

    private static final String GET="GET";
    private static final String POST="POST";
    private HashMap<String,String> apiParameters;
    private static String WebService;

    public CallWebService(String MethodType, String WebService, HashMap<String, String> apiParameters) throws MethodNotDefinedException {

        if(isNetConnectionAvailable()) {
                InternetConnectionAvailable();
                if (MethodType != null) {
                    if (MethodType.equalsIgnoreCase(GET)) {
                        if (WebService != null) {
                            this.WebService = WebService;
                            new ExecuteAsyncTask(GET).execute();
                        } else {
                            throw new NullPointerException("Please define webservice url.");
                        }
                    } else if (MethodType.equalsIgnoreCase(POST)) {
                        if (WebService != null) {
                            if (apiParameters != null) this.apiParameters = apiParameters;
                            this.WebService = WebService;
                            new ExecuteAsyncTask(POST).execute(apiParameters);
                        } else {
                            throw new NullPointerException("Please define webservice url.");
                        }
                    } else {
                        throw new MethodNotDefinedException("Define method for webservice.");
                    }
                } else {
                    throw new MethodNotDefinedException("Define method for webservice.");
                }
        }else{
            InternetConnectionNotAvailable();
        }
    }

    private class ExecuteAsyncTask extends AsyncTask<HashMap<String,String>,Void,JSONObject> {
        String MethodType;
        public ExecuteAsyncTask(String MethodType){
            this.MethodType=MethodType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            OnStartingService();

        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            try {
                OnGettingResult(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (MethodNotDefinedException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected JSONObject doInBackground(HashMap<String, String>... params) {
            if(MethodType.equalsIgnoreCase(GET))
            return getJSON(WebService,15000);
            else try {
                return getJSONPOST(WebService,params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void OnStartingService(){
    }
    public void OnGettingResult(JSONObject jsonObject) throws JSONException, MethodNotDefinedException {
    }
    public void InternetConnectionNotAvailable(){
    }
    public void InternetConnectionAvailable(){

    }

    private JSONObject getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    return new JSONObject(sb.toString());
            }

        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                }
            }


        }
        return null;
    }


    private JSONObject getJSONPOST(String url1, HashMap<String,String> apiParams) throws IOException, JSONException {
        URL url = new URL(url1);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setConnectTimeout(15000);
        conn.setReadTimeout(15000);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        String getParams=getQuery(apiParams);

        Log.d("++++++apiparam","+++ "+apiParams);
        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(getParams);
        writer.flush();
        writer.close();
        os.close();
        conn.connect();

        int status = conn.getResponseCode();


        switch (status) {
            case 200:
            case 201:
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line+"\n");
                }
                br.close();

                Log.e("++++","++++"+sb.toString());

                return new JSONObject(sb.toString());
        }

        if (conn != null) {
            try {
                conn.disconnect();
            } catch (Exception ex) {
            }
        }
        return null;
    }

    private String getQuery(HashMap<String,String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;


        for(Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    public static boolean isNetConnectionAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) PaginationDemo.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
