package animation.conceptioni.com.paginationdemo;

public class Constants {

    public static String apiUrl = "http://www.imcproduct.com/";

    public static String productListUrl = apiUrl+"home/product_list";
//
    public interface method {
        String POST = "POST";
        String GET = "GET";
    }
    public interface webServiceParam {
        String success = "success";
        String message = "message";
    }
}
